<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'body',
			[ // the status name 
				'label' => $model->attributeLabels()['categoryId'],
				'value' => $model->categoryItem->category_name,	
			],	
           [ // the status name 
				'label' => $model->attributeLabels()['author'],
				'format' => 'html',
				'value' => Html::a($model->userOwner->name, 
					['user/view', 'id' => $model->userOwner->id]),				
			],
			
               [ // the status name 
				'label' => $model->attributeLabels()['statusId'],
				'value' => $model->statusItem->status_name,	
			],
          	[ // Lead created by
				'label' => $model->attributeLabels()['created_by'],
				'value' => isset($model->createdBy->name) ? $model->createdBy->name : 'No one!',	
			],
			[ // Lead created at
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],				
			[ // Lead updated by
				'label' => $model->attributeLabels()['updated_by'],
				'value' => isset($model->updateddBy->name) ? $model->updateddBy->name : 'No one!',	
			],
			[ // Lead updated at
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],		
        ],
    ]) ?>

</div>
